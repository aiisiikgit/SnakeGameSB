// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"

// ����� ASnakeBase �������� ������� ������� ��� ������ � ����. � ������������ ������ ��������������� ��������� ��������� ������, ����� ��� ������ �������� ������ (ElementSize), �������� ������������ ������ (MovementSpeed) � ��������� ����������� �������� (LastMoveDirection).
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	//������� ���� *2+1
	BoardLimitX = 8; 
	BoardLimitY = 8;
	MovementSpeed = 0.5f;
	NextMoveDirection = EMovementDirection::DOWN;
	LastMoveDirection = NextMoveDirection;
	
}

// � ������ BeginPlay() ���������� ��������� ��������� ������. ����� ��������������� �������� ������� ����� �������� ������ Tick() � ������� ������ SetActorTickInterval() � ��������� ��������� �������� ������ � ������� ������ AddSnakeElement().
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(4);
	SpawnWall(0);
}

// ����� Tick() ���������� ������ ����� � ������������ ����������� ������ � ������� ������ Move().
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

// ����� AddSnakeElement() ������� ����� �������� ������ � ��������� �� � ������ SnakeElements. ��� ������� ������ �������� �������� ��� ��������� � ��������� ��������� ������ ASnakeElementBase.
void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FTransform NewTransform;
		if (ElementsNum > 1) {
			FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
			NewTransform = FTransform(NewLocation);
		}
		else {
			FVector NewLocation(SnakeElements[SnakeElements.Num()-1]->GetActorLocation());
			NewTransform = FTransform(NewLocation);
		}
			

		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this; // �������� ��������� ��� ��������
		int32 ElementIndex = SnakeElements.Add(NewSnakeElem);
		if (ElementIndex == 0) {
			NewSnakeElem->SetFirstElementType();
		}
	}
}

void ASnakeBase::SpawnWall(int LevelType)
{
	FTransform NewTransform;
	
	for (int x = -BoardLimitX-1; x <= BoardLimitX+1; ++x) {
		FVector NewLocation(x * ElementSize, (BoardLimitY + 1) * ElementSize, 0);
		NewTransform = FTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		
	}
	for (int x = -BoardLimitX-1; x <= BoardLimitX+1; ++x) {
		FVector NewLocation( x * ElementSize, -(BoardLimitY + 1) * ElementSize, 0);
		NewTransform = FTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
	}
	for (int y = -BoardLimitY-1; y <= BoardLimitY+1; ++y) {
		FVector NewLocation((BoardLimitX + 1) * ElementSize, y * ElementSize, 0);
		NewTransform = FTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
	}
	for (int y = -BoardLimitY-1; y <= BoardLimitY+1; ++y) {
		FVector NewLocation(- (BoardLimitX+1) * ElementSize, y * ElementSize, 0);
		NewTransform = FTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
	}

}

/*
void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(0, 0, 0);
		if (SnakeElements.Num() > 0) {
			NewLocation = SnakeElements.Last()->GetActorLocation() - FVector(ElementSize, 0, 0);
			}
		FTransform NewTransform = FTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElementIndex = SnakeElements.Add(NewSnakeElem);
		if (ElementIndex == 0) {
			NewSnakeElem->SetFirstElementType();
		}
	}
}*/
// ����� Move() ������������ ����������� ������. ������� ����������� ������ �������� � ����������� �� ����������� �������� ������. ����� ������ ������� ������ ������������ �� ����� �����, � ��������� �������� ������ ������������ �� ����� ����������� ��������. ������ ������� ������ ����� ������ ��������� �������� � ������� ������ ToggleCollision().
void ASnakeBase::Move()
{
	FVector MovementVector(FVector::ZeroVector);
	
	switch (NextMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	default:
		break;
	}
	LastMoveDirection = NextMoveDirection;
	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();


	for (int i = SnakeElements.Num() - 1; i > 0; i--) {
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}
	SnakeElements[0]->AddActorWorldOffset(MovementVector); //SetActorLocation(GetActorLocation)+MovementVector
	SnakeElements[0]->ToggleCollision();
}

// ����� SnakeElementOverlap() ������������ ������������ �������� ������ � ������ �������� �� �����. ���� ������������� ������ ������������ ��������� IInteractable, �� ���������� ����� Interact() � ����� �������. � �������� ��������� ���������� ������ �� ������� ��������� ������ ASnakeBase � ����, �����������, �������� �� ������������� ������� ������ � ������.*/
void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement)) {
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}