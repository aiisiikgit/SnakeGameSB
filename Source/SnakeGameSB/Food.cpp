// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "Math/UnrealMathUtility.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	FoodType = Default;
	DestroyTime = 1001;
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (DestroyTime < 1000) {
		DestroyTime = DestroyTime - DeltaTime;
		if (DestroyTime <= 0) {
			this->Destroy();
		}
	}
}



void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	
		
	if (IsValid(Snake)) {
		if (bIsHead) {
			Snake->AddSnakeElement();
		}
		// Relocate the food	
		float ElementSize = Snake->ElementSize;
		//int32 BoardLimitCells = Snake->BoardLimitX;
		FVector RandomLocation(FMath::RandRange(-(Snake->BoardLimitX), Snake->BoardLimitX) * ElementSize, FMath::RandRange(-(Snake->BoardLimitY), Snake->BoardLimitY) * ElementSize, GetActorLocation().Z);
		SetActorLocation(RandomLocation);
	}
	
}

